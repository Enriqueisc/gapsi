//
//  ProductModel.swift
//  GAPSI
//
//  Created by Kapital on 11/03/21.
//

import Foundation
import ObjectMapper
import RealmSwift
import ObjectMapperAdditions
import ObjectMapper_Realm


class ProductsResponse: Object, Mappable {
    @objc dynamic var total: Int = 0
    @objc dynamic var page: Int = 0
    var items = List<Item>()
    
    required convenience init?(map: Map) {
        self.init()
    }
    func mapping(map: Map) {
        self.total <- map["total"]
        self.page <- map["page"]
        self.items <- (map["items"], ListTransform<Item>())
    }
}



class Item: Object, Mappable {
    @objc dynamic var id: String = ""
    @objc dynamic var rating: Double = 0.0
    @objc dynamic var price: Double = 0.0
    @objc dynamic var image: String = ""
    @objc dynamic var title: String = ""
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        self.id <- map["id"]
        self.rating <- map["rating"]
        self.price <- map["price"]
        self.title <- map["title"]
        self.image <- map["image"]
    }
}
