//
//  StoreViewController.swift
//  GAPSI
//
//  Created by Kapital on 11/03/21.
//

import UIKit
import PKHUD

class StoreViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    let searchController = UISearchController(searchResultsController: nil)
    var products: [Item] = []
    var productsFiltered: [Item] = []
    var filtersEnabled: Bool = false
    var textFilter: String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.prefersLargeTitles = true
        // Do any additional setup after loading the view.
        self.tableView.register(UINib(nibName: "ItemTableViewCell", bundle: nil), forCellReuseIdentifier: "ItemTableViewCell")
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.refreshControl = UIRefreshControl()
        self.tableView.refreshControl?.tintColor = .white
        self.tableView.refreshControl?.addTarget(self, action:
                                              #selector(didRefresh),
                                              for: .valueChanged)
        listProducts()
        setupSearchController()
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if searchController.searchBar.text?.isEmpty ?? false{
            self.textFilter = ""
            self.searchController.searchBar.text = self.textFilter
            listProducts()
        }else{
            self.listProducts()
        }
    }
    
    
    func setupSearchController(){
        self.searchController.searchResultsUpdater = self
        self.searchController.obscuresBackgroundDuringPresentation = false
        self.searchController.searchBar.placeholder = "Buscar..."
        self.searchController.searchBar.showsSearchResultsButton = false
        self.searchController.searchBar.delegate = self
        //self.searchController.searchBar.enablesReturnKeyAutomatically = false
        if #available(iOS 11.0, *) {
            self.navigationItem.searchController = self.searchController
        } else {
            // Fallback on earlier versions
        }
        definesPresentationContext = true
    }
    
    
    //Pull to refresh
    @IBAction func didRefresh(_ sender: UIRefreshControl) {
        if searchController.searchBar.text?.isEmpty  ?? false {
            self.textFilter = ""
            self.searchController.searchBar.text = self.textFilter
            sender.endRefreshing()
        }else{
            self.textFilter = ""
            self.searchController.searchBar.text = self.textFilter
            listProducts()
            sender.endRefreshing()
        }
        
    }
    
    
    func listProducts(){
        print("TextFilter: ", textFilter)
        HUD.show(.progress)
        getProducts(query: "\(textFilter)"){response, error in
            if error == nil {
                if let products = response?.items {
                    HUD.flash(.success, delay: 0.0)
                    self.products = products.reversed()
                    self.tableView.reloadData()
                }else{
                    HUD.flash(.error, delay: 1.0)
                    self.products = []
                    self.tableView.reloadData()
                }
            }else{
                HUD.flash(.error, delay: 1.0)
                self.products = []
                self.tableView.reloadData()
            }
        }
    }
    
    func searchBarIsEmpty() -> Bool {
        return self.searchController.searchBar.text?.isEmpty ?? true
    }
    
    
    func isFiltering() -> Bool {
        if self.searchController.isActive && !searchBarIsEmpty() {
            return true
        } else if self.filtersEnabled {
            return true
        } else {
            return false
        }
    }
    
    
    
    @IBAction func unwindToUpdateProducts(sender: UIStoryboardSegue) {
        print("Reactualiza Lista")
        if let source = sender.source as? SearchViewController{
            print("Busqueda Seleccionda: ",  source.selectedQuery)
            self.textFilter = source.selectedQuery
            self.searchController.searchBar.text = source.selectedQuery
            self.listProducts()
        }else{
            print("no hay nada")
        }
    }

}


extension StoreViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.products.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ItemTableViewCell", for: indexPath) as! ItemTableViewCell
        let applyFilters = self.products[indexPath.row]
        let item = applyFilters
        cell.setupCell(item: item)
        return cell
    }
    
    
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        //Scroll up velocity
        if(velocity.y < 0){
            if self.textFilter.isEmpty {
                //Ocultar el teclado para visualizar los paises en la lista
                self.searchController.searchBar.endEditing(true)
            }else{
                print("Tiene filtro de busqueda y hace scroll")
            }
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 68
    }
    
    
    
}


extension StoreViewController: UISearchResultsUpdating, UISearchBarDelegate {
    // MARK: - UISearchResultsUpdating Delegate
    func updateSearchResults(for searchController: UISearchController) {
        if let text = searchController.searchBar.text {
            if isFiltering(){
                if text.isEmpty{
                    self.textFilter = ""
                    self.tableView.reloadData()
                }else{
                    self.textFilter = text
                    self.tableView.reloadData()
                }
            }else{
                self.textFilter = ""
                self.tableView.reloadData()
            }
        }
    }
    
    
    
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.performSegue(withIdentifier: "to_search", sender: nil)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        if searchBar.text?.isEmpty ?? false {
            searchBar.showsCancelButton = false
            self.textFilter = ""
            searchBar.text = self.textFilter
        }else{
            searchBar.showsCancelButton = false
            self.textFilter = ""
            searchBar.text = self.textFilter
            //Reiniciar la busqueda
            print("Cancelar la busqueda")
            self.listProducts()
        }
    }
}
