//
//  ItemTableViewCell.swift
//  GAPSI
//
//  Created by Kapital on 11/03/21.
//

import UIKit
import Kingfisher

class ItemTableViewCell: UITableViewCell {
    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var price: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    
    func setupCell(item: Item){
        self.name.text = "\(item.title)"
        //self.logo.layer.cornerRadius = self.logo.frame.width / 2
        self.logo.kf.setImage(with: URL(string: item.image))
        self.price.text = "\(item.price)".currencyDecimals
    }
    
}
