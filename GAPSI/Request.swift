//
//  Request.swift
//  CountryApp
//
//  Copyright © 2021 Enrique. All rights reserved.
//

import UIKit
import Foundation
import Alamofire
import AlamofireObjectMapper
import ObjectMapper
import ObjectMapperAdditions
import ObjectMapper_Realm
import RealmSwift



func getProducts(query: String?, completion: @escaping(ProductsResponse?, String?) -> Void) {
    let queryStr = query?.isEmpty ?? false ? "" : "?&query=\(query ?? "")"
    let headers = [
        "X-IBM-Client-Id": "\(Constants.KEY_API)"
    ]
    print("query : ", queryStr)
    let urlApi = "\(Constants.URL_API)/search\(queryStr)"
    print(urlApi)
    Alamofire.request("\(urlApi)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers).responseObject { (response: DataResponse<ProductsResponse>) in
        let statusCode = response.response?.statusCode ?? 0
        if statusCode < 300{
            guard let resp = response.result.value else {
                completion(nil, "Error al consultar productos")
                return
            }
            print("Respuesta: ", resp)
            completion(resp, nil)
        }else{
            guard let data = response.data else {
                completion(nil, "Error al consultar productos")
                return
            }
            do {
                if let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? NSDictionary, let customError = json["message"] as? String {
                    completion(nil, customError)
                } else {
                    completion(nil, "Error al consultar productos")
                }
            } catch let error {
                print("el error es \(error)")
                completion(nil, "Error al consultar productos")
            }
        }
    }
}
