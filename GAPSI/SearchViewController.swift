//
//  SearchViewController.swift
//  GAPSI
//
//  Created by Kapital on 12/03/21.
//

import UIKit
import SDStateTableView

class SearchViewController: UIViewController, UISearchBarDelegate{
    let searchBar = UISearchBar()
    @IBOutlet weak var tableView: SDStateTableView!
    
    var history:[String] = []
    var selectedQuery: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib(nibName: "LabelTableViewCell", bundle: nil), forCellReuseIdentifier: "LabelTableViewCell")
        self.getHistory()
        setUpNavBar()
    }
    
    
    func getHistory(){
        //Pregunta si existe un objeto HISTORY
        if UserDefaults.contains("\(Constants.HISTORY_SEARCH)"){
            //Existe la lista de busquedas
            if let listHistory = UserDefaults.standard.stringArray(forKey: "\(Constants.HISTORY_SEARCH)"){
                print("Lista: ", listHistory)
                self.history = listHistory
                //self.tableView.reloadData()
                self.tableView.separatorColor = .lightGray
                self.tableView.setState(.dataAvailable)
            }else{
                print("No existe el historico")
                self.tableView.separatorColor = .clear
                self.tableView.setState(.withButton(errorImage: UIImage(named: "no_data"), title: "No existe historial", message: "", buttonTitle: "Consultar nuevamente", buttonConfig: { (button) in }, retryAction: {
                    print("Start")
                }))
            }
        }else{
            print("No existe el historico")
            self.tableView.separatorColor = .clear
            self.tableView.setState(.withButton(errorImage: UIImage(named: "no_data"), title: "No existe historial", message: "", buttonTitle: "Consultar nuevamente", buttonConfig: { (button) in }, retryAction: {
                print("Start")
            }))
        }
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        searchBar.becomeFirstResponder()
    }
    
    func setUpNavBar() {
        searchBar.sizeToFit()
        searchBar.searchBarStyle = .minimal
        searchBar.placeholder = "Buscar..."
        searchBar.tintColor = UIColor.lightGray
        searchBar.barTintColor = UIColor.lightGray
        navigationItem.titleView = searchBar
        searchBar.isTranslucent = true
        searchBar.delegate = self
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        print("Buscar Ahora",searchBar.text ?? "")
        if (searchBar.text?.count ?? 0 < 2) {
            return;
        }
        else {
            print("Buscar Algo: ", searchBar.text ?? "")
            saveWordHistory(text: searchBar.text ?? "")
        }
    }
    
    
    
    func saveWordHistory(text: String){
        //Verificar que la cadena no exista en el historico
        if history.contains(where: {$0.lowercased() == text.lowercased() }){
            print("Si existe, Realiza la busqueda")
            findProducts(textSearch: text)
        }else{
            print("No contiene la query en el array")
            history.append(text)
            updateHistory(history: history)
            print("Realiza la busqueda")
            findProducts(textSearch: text)
        }
    }
    
    func findProducts(textSearch: String){
        self.selectedQuery = textSearch
        self.performSegue(withIdentifier: "unwindToUpdateProducts", sender: self)
    }
    
    
    func updateHistory(history: [String]){
        let defaults = UserDefaults.standard
        defaults.set(history, forKey: "\(Constants.HISTORY_SEARCH)")
        defaults.synchronize()
    }
    
    
    func deleteWord(text: String){
        print("Delete: ", text)
        if let indexOfFirstSuchElement = history.index(where: { $0.lowercased() == text.lowercased() }) {
            history.remove(at: indexOfFirstSuchElement)
            updateHistory(history: history)
            if history.count > 0 {
                self.tableView.reloadData()
            }else{
                self.tableView.separatorColor = .clear
                self.tableView.setState(.withButton(errorImage: UIImage(named: "no_data"), title: "No existe historial", message: "", buttonTitle: "Consultar nuevamente", buttonConfig: { (button) in }, retryAction: {
                    print("Start")
                }))
            }
            
        }
    }
    
    
    @objc func delete(sender : UIButton){
        let text = self.history[sender.tag]
        deleteWord(text: text)
    }

}

extension SearchViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            switch (tableView as! SDStateTableView).currentState {
                case .dataAvailable:
                    return self.history.count
                default:
                    return 0
            }
        }else{
            return 0
        }
        
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let textSearch = self.history[indexPath.row].lowercased()
        let cell = tableView.dequeueReusableCell(withIdentifier: "LabelTableViewCell", for: indexPath) as! LabelTableViewCell
        cell.query.text = textSearch
        cell.btnDelete.tag = indexPath.row
        cell.btnDelete.addTarget(self,action: #selector(self.delete(sender:)),for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let textSearch = self.history[indexPath.row].lowercased()
        findProducts(textSearch: textSearch)
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    
}
