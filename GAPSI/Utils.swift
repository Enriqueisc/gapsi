//
//  Utils.swift
//  GAPSI
//
//  Created by Kapital on 11/03/21.
//

import Foundation

extension String {
    var currency: String {
        let stringWithoutSymbol = self.replacingOccurrences(of: "$", with: "")
        let stringWithoutComma = stringWithoutSymbol.replacingOccurrences(of: ",", with: "")
        let formatter = NumberFormatter()
        formatter.currencySymbol = "$"
        formatter.minimumFractionDigits = 0
        formatter.maximumFractionDigits = 0
        formatter.numberStyle = .currency
        formatter.locale = Locale(identifier: "en_US")
        
        if let result = NumberFormatter().number(from: stringWithoutComma) {
            return formatter.string(from: result)!
        }

        return self
    }
    
    var currencyDecimals: String {
        let stringWithoutSymbol = self.replacingOccurrences(of: "$", with: "")
        let stringWithoutComma = stringWithoutSymbol.replacingOccurrences(of: ",", with: "")
        let formatter = NumberFormatter()
        formatter.currencySymbol = "$"
        formatter.minimumFractionDigits = 2
        formatter.maximumFractionDigits = 2
        formatter.numberStyle = .currency
        formatter.locale = Locale(identifier: "en_US")
        
        if let result = NumberFormatter().number(from: stringWithoutComma) {
            return formatter.string(from: result)!
        }

        return self
    }
}

extension UserDefaults {
    static func contains(_ key: String) -> Bool {
        return UserDefaults.standard.object(forKey: key) != nil
    }
}
